# LogCraft-cli Detection as Code CI/CD Example

This repository serves as a demonstration of a detection-as-code pipeline using logcraft-cli.
Resources
- [LogCraft-cli repository](https://github.com/logcraft-cli/logcraft-cli)
- [LogCraft-cli documentation](https://docs.logcraft.io/logcraft-cli/what-is-logcraft-cli.html)

## Features

- **Integration with logcraft-cli:** Demonstrates how to incorporate logcraft-cli into a GitLab CI/CD pipeline.
- **State management:** Uses GitLab-managed Terraform state for consistent state management.
- **Example configuration:** Includes sample configuration files and scripts to set up and run logcraft-cli within a CI environment.

## Usage

### Prerequisites

Before getting started, ensure the following prerequisites are met:

- GitLab account with access to create projects and pipelines.

### Setting Up (Simple Example)
1. **Create a Gitlab Repository**
2. **Clone your Repository:**
    ```
    git clone https://gitlab.com/your-username/my-detections.git
3. **Copy this [Gitlab CI/CD file](.gitlab-ci.yml)  to your repository:**
    ```yaml
        include:
          - 'https://gitlab.com/logcraft/ci-templates/-/raw/main/LogCraft.gitlab-ci.yml'

        variables:
          # Uses the branch name for the environment ID.
          LGC_ENV_ID: $CI_COMMIT_REF_NAME

        .env_rules:
          - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
            # If merge request event, use the target branch name.
            variables:
              LGC_ENV_ID: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
          - if: $CI_COMMIT_REF_PROTECTED == "true"
            variables:
              LGC_ENV_ID: $CI_COMMIT_REF_NAME

        stages:
          - validate
          - diff
          - deploy
          - destroy

        diff:
          extends: .lgc:diff
          rules:
            - !reference [ .env_rules ]
    ```
4. **Setup the project:**
    ```bash
    lgc init
5. **Configure the project**

    To make full use of Gitlab, you will need to configure the project with the following:
    ```yaml
    state:
      type: Http # <-- Use the Http backend state provider to share state accross pipelines 
    plugins: {}
    environments: []
    services: []
    ```
6. **Add the plugins/services/environments you need**
7. **Create your rules in `./rules` folder**


### Customization

You can customize the pipeline by modifying the `.gitlab-ci.yml` file to match your needs.<br>
Templates are located in this repository: [LogCraft ci-templates](https://gitlab.com/logcraft/ci-templates) and can be overridden.

Here is an example of how to override the template:
```yaml
include:
  - 'https://gitlab.com/logcraft/ci-templates/-/raw/main/LogCraft.gitlab-ci.yml'

stages:
  - validate
  - diff
  - deploy
  - destroy

variables:
  # Uses the branch name for the environment ID.
  LGC_ENV_ID: $CI_COMMIT_REF_NAME

.env_rules:
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    variables:
      # If default branch use `prod` as environment ID.
      LGC_ENV_ID: prod

diff:
  extends: .lgc:diff
  rules:
  - !reference [ .env_rules ]

deploy:
  extends: .lgc:deploy
  rules:
  - !reference [ .env_rules ]

destroy:
  extends: .lgc:destroy
  rules:
  - !reference [ .env_rules ]
```